package org.sid;

import org.sid.dao.ContactRepository;
import org.sid.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 
 
@SpringBootApplication
public class ContactsApplication implements CommandLineRunner{
	@Autowired
	private ContactRepository contactRepository;

	public static void main(String[] args) {
		SpringApplication.run(ContactsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		 contactRepository.save(new Contact("MESKY", "Rachid", 28));
		 contactRepository.save(new Contact("MBAPI", "Taha", 24));
		 contactRepository.save(new Contact("MESSY", "Leo", 36));
		 contactRepository.findAll().forEach(c -> {
			 System.out.println(c.getNom());
			 System.out.println(c.getPrenom());
			 System.out.println(c.getAge());
			 
		 });
		
	}

}
